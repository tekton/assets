<?php namespace Tekton\Support\Facades;

class Asset extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'assets'; }
}
